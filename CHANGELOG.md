# CHANGELOG.md

### v0.0.3 (Aug 2017) -- Ilya Frolov (ilya@gitlab.com)
 * Add functionality for creating gitaly memory controller
 * Add functionality for pgrepping and adding all descendent pids to that controller

### v0.0.2 (Aug 2017) -- Ilya Frolov (ilya@gitlab.com)
 * Initial commit of the cookbook from template.
