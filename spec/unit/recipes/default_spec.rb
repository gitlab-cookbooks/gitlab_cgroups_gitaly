# frozen_string_literal: true

# Cookbook:: gitlab_cgroups_gitaly
# Spec:: default
#
# Copyright:: 2020, GitLab Inc., MIT.

require 'spec_helper'
describe 'gitlab_cgroups_gitaly::default' do
  context 'when all attributes are default, on Ubuntu 16.04' do
    let(:chef_run) do
      ChefSpec::ServerRunner.new(platform: 'ubuntu',
                                 version: '16.04')
                            .converge(described_recipe)
    end

    it 'converges succesfully' do
      expect { chef_run }.to_not raise_error
    end

    it 'updates apt' do
      expect(chef_run).to periodic_apt_update('apt update')
    end

    it 'installs cgroup-tools' do
      expect(chef_run).to install_package('cgroup-tools')
    end

    it 'creates /etc/cgconfig.conf' do
      content = File.read('spec/fixtures/cgconfig.conf')

      expect(chef_run).to render_file('/etc/cgconfig.conf').with_content(content)
      expect(chef_run.template('/etc/cgconfig.conf')).to notify('execute[cgconfigparser]').to(:run).immediately
    end

    it 'creates cgconfig.service' do
      content = File.read('spec/fixtures/cgconfig.service')

      expect(chef_run).to create_systemd_unit('cgconfig.service').with(content: content)
    end

    it 'creates a wrapper script' do
      path = '/usr/local/bin/git-cgexec-wrapper'

      expect(chef_run).to create_template(path).with(mode: '0777')
      expect(chef_run).to render_file(path).with_content(<<~BASH
        #!/bin/bash
        checksum=( $(/usr/bin/cksum <<<"$@") )
        slot=$(( ${checksum[0]} % 2 ))
        /usr/bin/cgexec -g "memory,cpu:gitaly/gitaly-${slot}" /opt/gitlab/embedded/bin/git "$@"
      BASH
                                                        )
    end
  end
end
