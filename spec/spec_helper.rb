# frozen_string_literal: true

require 'chefspec'
require 'chefspec/berkshelf'

RSpec.configure do |c|
  c.before do
    stub_command(/^test .*/).and_return(false)
  end
end

ChefSpec::Coverage.start!
