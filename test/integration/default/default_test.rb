# frozen_string_literal: true

# InSpec tests for recipe gitlab_cgroups_gitaly::default
control 'general-checks' do
  impact 1.0
  title 'General tests for gitlab_cgroups_gitaly cookbook'
  desc '
    This control ensures that cookbook:
      * creates the container groups
      * sets memory.limit_in_bytes inside each group correctly
      * sets cpu.shares inside each group correctly
  '

  2.times do |i|
    describe directory("/sys/fs/cgroup/memory/gitaly/gitaly-#{i}") do
      it { should exist }
      its('owner') { should eq 'syslog' }
      its('group') { should eq 'syslog' }
    end

    describe directory("/sys/fs/cgroup/cpu/gitaly/gitaly-#{i}") do
      it { should exist }
      its('owner') { should eq 'syslog' }
      its('group') { should eq 'syslog' }
    end

    describe file("/sys/fs/cgroup/memory/gitaly/gitaly-#{i}/memory.limit_in_bytes") do
      it { should exist }
      its('owner') { should eq 'syslog' }
      its('group') { should eq 'syslog' }
      its('content') { should eq "#{1 * 1024 * 1024 * 1024}\n" }
    end

    describe file("/sys/fs/cgroup/cpu/gitaly/gitaly-#{i}/cpu.shares") do
      it { should exist }
      its('owner') { should eq 'syslog' }
      its('group') { should eq 'syslog' }
      its('content') { should eq "1024\n" }
    end
  end

  # try to eat 10G of ram in a child process, adding it to cgroup beforehand
  # that's why sh is embedded, as adding current connection there might kill it
  # TODO: apt-get should be part of converge process, but I can't find _how_
  describe bash("apt-get update; apt-get -y -q install stress; sh -c 'echo $$ > /sys/fs/cgroup/memory/gitaly/gitaly-1; stress -q --vm 1 --vm-bytes 10G'") do
    its('exit_status') { should eq 1 }
    its('stderr') { should include 'malloc failed: Cannot allocate memory' }
  end
end
