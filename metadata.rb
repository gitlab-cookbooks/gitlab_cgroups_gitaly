# frozen_string_literal: true

name             'gitlab_cgroups_gitaly'
maintainer       'GitLab Inc.'
maintainer_email 'ops-contact+cookbooks@gitlab.com'
license          'MIT'
description      'Manages cgroups for Gitaly'
version          '1.0.0'
chef_version     '>= 12.1'
issues_url       'https://gitlab.com/gitlab-cookbooks/gitlab_cgroups_gitaly/issues'
source_url       'https://gitlab.com/gitlab-cookbooks/gitlab_cgroups_gitaly'

supports 'ubuntu', '= 16.04'
