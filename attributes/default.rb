# frozen_string_literal: true

default['gitlab_cgroups_gitaly']['gitaly_user'] = 'git'
default['gitlab_cgroups_gitaly']['gitaly_group'] = 'git'

default['gitlab_cgroups_gitaly']['git_bin_path'] = '/opt/gitlab/embedded/bin/git'
default['gitlab_cgroups_gitaly']['wrapper_script_path'] = '/usr/local/bin/git-cgexec-wrapper'

default['gitlab_cgroups_gitaly']['containers']['count'] = 2
default['gitlab_cgroups_gitaly']['containers']['memory']['limit_in_bytes'] = 1 * 1024 * 1024 * 1024
default['gitlab_cgroups_gitaly']['containers']['cpu']['shares'] = 1024
