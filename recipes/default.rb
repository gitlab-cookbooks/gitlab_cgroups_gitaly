# frozen_string_literal: true

# Cookbook:: gitlab_cgroups_gitaly
# Recipe:: default
# License:: MIT
#
# Copyright:: 2020, GitLab Inc.

gitaly_user = node['gitlab_cgroups_gitaly']['gitaly_user']
gitaly_group = node['gitlab_cgroups_gitaly']['gitaly_group']
wrapper_script_path = node['gitlab_cgroups_gitaly']['wrapper_script_path']
containers_count = node['gitlab_cgroups_gitaly']['containers']['count']

apt_update 'apt update'

package 'cgroup-tools'

template '/etc/cgconfig.conf' do
  source 'cgconfig.conf.erb'
  variables(
    containers_count: containers_count,
    gitaly_user: gitaly_user,
    gitaly_group: gitaly_group,
    cpu_shares: node['gitlab_cgroups_gitaly']['containers']['cpu']['shares'],
    memory_limit_in_bytes: node['gitlab_cgroups_gitaly']['containers']['memory']['limit_in_bytes']
  )
  notifies :run, 'execute[cgconfigparser]', :immediately
end

execute 'cgconfigparser' do
  command '/usr/sbin/cgconfigparser -l /etc/cgconfig.conf -s 1664'
  action :nothing
end

systemd_unit 'cgconfig.service' do
  # Adapted from https://aur.archlinux.org/packages/libcgroup/
  content <<~STR
    [Unit]
    Description=Control Group configuration service

    # The service should be able to start as soon as possible,
    # before any 'normal' services:
    DefaultDependencies=no
    Conflicts=shutdown.target
    Before=basic.target shutdown.target

    [Service]
    Type=oneshot
    RemainAfterExit=yes
    ExecStart=/usr/sbin/cgconfigparser -l /etc/cgconfig.conf -s 1664
    ExecStop=/usr/sbin/cgclear -l /etc/cgconfig.conf -e

    [Install]
    WantedBy=sysinit.target
  STR

  action %i[create enable start]
end

template wrapper_script_path do
  source 'wrapper_script.erb'
  variables(
    containers_count: containers_count,
    git_bin_path: node['gitlab_cgroups_gitaly']['git_bin_path']
  )
  mode '0777'
end

node.default['omnibus-gitlab']['gitlab_rb']['gitaly']['git_bin_path'] = wrapper_script_path
